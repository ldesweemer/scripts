#!/bin/bash

set -u

FONT_SIZE=12

install_powerline_fonts() {
  # install Powerline fonts
  echo "installing powerline fonts..."
  TMP_DIR=`mktemp -d`
  git clone https://github.com/powerline/fonts.git "$TMP_DIR"
  sh $TMP_DIR/install.sh
  rm -rf "$TMP_DIR"
}

append_config() {
  cat << end_file >> ~/.Xresources
xterm*termName:          xterm-256color
xterm*selectToClipboard: true
xterm*locale:            true
xterm*background:        black
xterm*foreground:        white
xterm*utf8:              1
xterm*scrollBar:         true
xterm*rightScrollBar:    true
xterm*faceName:          Inconsolata for Powerline:size=${FONT_SIZE}:antialias=true:hinting=true
xterm*color0:            #000000
xterm*color1:            #B40000
xterm*color2:            #00AA00
xterm*color3:            #AAAA00
xterm*color4:            #5555AA
xterm*color5:            #AA00AA
xterm*color6:            #00AAAA
xterm*color7:            #AAAAAA
xterm*color8:            #555555
xterm*color9:            #FF0000
xterm*color10:           #00FF00
xterm*color11:           #FFFF00
xterm*color12:           #6464FF
xterm*color13:           #FF00FF
xterm*color14:           #00FFFF
xterm*color15:           #FFFFFF

URxvt*scrollBar:         true
URxvt*scrollBar_right:   true
URxvt*transparent:       true
URxvt*background:        black
URxvt*foreground:        white
URxvt*shading:           10
URxvt.saveLines:         10000
URxvt.xftAntialias:      true
URxvt.font:              xft:Inconsolata for Powerline:size=${FONT_SIZE}:antialias=true:hinting=true
URxvt.boldFont:          xft:Inconsolata for Powerline:size=${FONT_SIZE}:antialias=true:hinting=true
URxvt.italicFont:        xft:Inconsolata for Powerline:size=${FONT_SIZE}:antialias=true:hinting=true
URxvt.bolditalicFont:    xft:Inconsolata for Powerline:size=${FONT_SIZE}:antialias=true:hinting=true
URxvt*color0:            #000000
URxvt*color1:            #B40000
URxvt*color2:            #00AA00
URxvt*color3:            #AAAA00
URxvt*color4:            #5555AA
URxvt*color5:            #AA00AA
URxvt*color6:            #00AAAA
URxvt*color7:            #AAAAAA
URxvt*color8:            #555555
URxvt*color9:            #FF0000
URxvt*color10:           #00FF00
URxvt*color11:           #FFFF00
URxvt*color12:           #6464FF
URxvt*color13:           #FF00FF
URxvt*color14:           #00FFFF
URxvt*color15:           #FFFFFF
end_file
}

configure_urxvt() {
  echo "configuring urxvt..."
  if [ ! -e "$HOME/.Xresources" ]; then
    touch ~/.Xresources
  else
    sed -i '/^\s*URxvt/d' ~/.Xresources
    sed -i '/^\s*xterm/d' ~/.Xresources
  fi
  append_config
  xrdb ~/.Xresources
}

err() {
  echo "$1" >&2
  exit 1
}

get_vars() {
  read -p "Font size ? (${FONT_SIZE}): " INPUT
  if [ -n "$INPUT" ]; then
    if ! test "$INPUT" -gt 0 2>/dev/null; then
      err "invalid font size, exiting !"
    fi
    FONT_SIZE=$INPUT
  fi
}

main() {
  get_vars
	install_powerline_fonts
  configure_urxvt
}

main "$@" || exit 1

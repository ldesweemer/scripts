#!/bin/bash

set -u

check_env() {
  # check if git is installed with python support  
  if ! command -v git 2>&1 > /dev/null; then
    err "git is not installed, exiting !"
  fi

  # check if vim is installed with python support  
  if ! command -v vim 2>&1 > /dev/null; then
    err "vim is not installed, exiting !"
  fi

  if ! vim --version | grep -q "+python"; then
   err "build vim with python support, exiting !"
  fi

  # check if pip is installed for python 2
  if ! command -v pip2 2>&1 > /dev/null; then
    err "pip must be installed for python2, exiting !"
  fi  

  # check if powerline-status is installed
  if python -c "import powerline" &> /dev/null; then
    # keep powerline-status up to date
    echo updating powerline-status...  
    sudo pip2 install --upgrade powerline-status
  else  
    echo installing powerline-status...  
    sudo pip2 install powerline-status
  fi    
}

err() {
  echo "$1" >&2
  exit 1
}

install_vundle() {
  echo installing vundle...  
  if [ ! -d "$HOME/.vim/bundle" ]; then
    echo creating bundle directory for vim...  
    mkdir -p ~/.vim/bundle  
  fi    

  if [ ! -d "$HOME/.vim/bundle/Vundle.vim" ]; then 
    echo installing vundle...
    git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
  else
    echo updating vundle...
    git -C ~/.vim/bundle/Vundle.vim pull origin master   
  fi  
}

create_vimrc() {
  echo creating vimrc file...
  cat << end_file > ~/.vimrc
set nocompatible
filetype off

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'
Plugin 'fatih/vim-go'
Plugin 'Shougo/neocomplete.vim'
Plugin 'nsf/gocode', {'rtp': 'vim/'}
Plugin 'scrooloose/nerdtree'
Plugin 'benmills/vimux'
call vundle#end()

" Activate powerline plugin installed with pip install powerline
python from powerline.vim import setup as powerline_setup
python powerline_setup()
python del powerline_setup

let g:go_highlight_functions = 1
let g:go_highlight_methods = 1
let g:go_highlight_fields = 1
let g:go_highlight_types = 1
let g:go_highlight_operators = 1
let g:go_highlight_build_constraints = 1

let g:neocomplete#enable_at_startup = 1 

let g:go_gocode_autobuild = 1
let g:go_gocode_propose_builtins = 1

let g:go_fmt_command = "goimports"

autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
map <C-n> :NERDTreeToggle<CR>
map <C-c> :VimuxPromptCommand<CR>
map <C-r> :VimuxRunLastCommand<CR>

filetype plugin indent on

autocmd Filetype go setlocal ts=4 sw=4 noexpandtab
autocmd Filetype sh setlocal ts=2 sw=2 expandtab

syntax enable
set nu
set mouse=a
set ts=4
set sw=4
set expandtab
colorscheme desert
end_file
}

install_plugins() {
  echo installing vim plugins..
  vim -i NONE -c VundleUpdate -c quitall  
}

main() {
  check_env
  install_vundle
  create_vimrc
  install_plugins
}

main "$@" || exit 1
